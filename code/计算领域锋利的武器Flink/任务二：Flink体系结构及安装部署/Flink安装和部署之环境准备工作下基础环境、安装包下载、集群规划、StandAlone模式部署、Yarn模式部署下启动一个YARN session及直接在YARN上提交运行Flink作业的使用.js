 1.Flink安装和部署之环境准备工作
   
   Flink支持多种安装模式
   local(本地)：单机模式，一般本地开发调试使用
   StandAlone 独立模式：Flink自带集群，自己管理资源调度，生产环境也会有所应用
   Yarn模式：计算资源统一由Hadoop YARN管理，生产环境应用较多
   1).基础环境
   jdk1.8及以上【配置JAVA_HOME环境变量】
   ssh免密码登录【集群内节点之间免密登录】
   2).安装包下载
   配套资料文件夹中提供，使用Flink1.11.1 版本
   3).集群规划
   linux121                 linux121    linux123
   JobManager+TaskManager TaskManager TaskManager
   4).StandAlone模式部署
   Step1、Flink安装包上传到linux121对应目录并解压
   Step2、修改 flink-1.11.1/conf/flink-conf.yaml 文件
   jobmanager.rpc.address: linux121
   taskmanager.numberOfTaskSlots: 2
   修改 flink-1.11.1/conf/masters
   linux121:2181
   Step3、修改 flink-1.11.1/conf/workers文件
linux121
linux122
linux123
   修改 flink-1.11.1/conf/zoo.cfg文件
# ZooKeeper quorum peers
server.1=linux121:2888:3888
server.2=linux122:2888:3888
server.3=linux123:2888:3888
   通过scp分发flink-1.11.1 文件到Linux122、linux123
   scp -r flink-1.11.1 linux122:$PWD
   scp -r flink-1.11.1 linux123:$PWD
   Step4、standalone模式启动
   bin目录下执行./start-cluster.sh
   Step5、jps进程查看核实
[root@linux121 bin]# jps
1616 StandaloneSessionClusterEntrypoint
2019 Jps
1917 TaskManagerRunner
   Step6、查看Flink的web页面 ip:8081/#/overview
   修改 flink-1.11.1/conf/flink-conf.yaml 文件
   taskmanager.numberOfTaskSlots: 2
   scp flink-conf.yaml linux122:$PWD
   scp flink-conf.yaml linux123:$PWD
   Step7、集群模式下运行example测试
   ./flink run -c WordCountScalaStream -p 2 /root/myjars/FirstFlink-1.0-SNAPSHOT.jar
   Step8、查看结果文件
   注意：集群搭建完毕后,Flink程序就可以达成Jar,在集群环境下类似于Step7中一样提交执行计算任务
   打jar包插件：
    <build>
        <plugins>
            <!-- 打jar插件 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>2.4.3</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <filters>
                                <filter>
                                    <artifact>*:*</artifact>
                                    <excludes>
                                        <exclude>META-INF/*.SF</exclude>
                                        <exclude>META-INF/*.DSA</exclude>
                                        <exclude>META-INF/*.RSA</exclude>
                                    </excludes>
                                </filter>
                            </filters>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
*/ 
   当并行度为1的时候，子任务为1，子任务运行在任务槽中
   5).Yarn模式部署
   (1).启动一个YARN session(Start a long-running Flink cluster on YARN)；
   修改/etc/profile
export HADOOP_CONF_DIR=$HADOOP_HOME
export YARN_CONF_DIR=$HADOOP_HOME/etc/hadoop
export HADOOP_CLASSPATH=`hadoop classpath`
   配置文件 yarn-site.xml
<property>
 <name>yarn.nodemanager.pmem-check-enabled</name>
 <value>false</value>
</property>
<property>
 <name>yarn.nodemanager.vmem-check-enabled</name>
 <value>false</value>
</property>
<property>
 <name>yarn.resourcemanager.address</name>
 <value>teacher2:8032</value>
</property>
<property>
 <name>yarn.resourcemanager.scheduler.address</name>
 <value>teacher2:8030</value>
</property>
<property>
 <name>yarn.resourcemanager.resource-tracker.address</name>
 <value>teacher2:8031</value>
</property>
   
   注意：yarn-site的修改需要在集群的每一台机器上执行
   启动hadoop （hdfs-yarn) yarn-session.sh -h
   /export/servers/flink/bin/yarn-session.sh -n 2 -tm 800 -s 1 -d
   申请2个CPU、1600M内存：：
   bin/yarn-session.sh -n 2 -tm 800 -s 1 -d
   # -n 表示申请2个容器，这里指的就是多少个taskmanager
   # -s 表示每个TaskManager的slots数量
   # -tm 表示每个TaskManager的内存大小
   # -d 表示以后台程序方式运行
   l 解释
   上面的命令的意思是，同时向Yarn申请3个container
   (即便只申请了两个，因为ApplicationMaster和Job Manager有一个额外的容器。一旦将Flink部署到
YARN群集中，它就会显示Job Manager的连接详细信息)
   2 个 Container 启动 TaskManager -n 2,每个 TaskManager 拥有1个 Task Slot -s 1,并且向每个
TaskManager 的 Container 申请 800M 的内存，以及一个ApplicationMaster--Job Manager。
   如果不想让Flink YARN客户端始终运行，那么也可以启动分离的 YARN会话。该参数被称为-d或--
detached。在这种情况下，Flink YARN客户端只会将Flink提交给集群，然后关闭它自己
   yarn-session.sh(开辟资源) + flink run(提交任务)
   - 使用Flink中的yarn-session(yarn客户端)，会启动两个必要服务JobManager和TaskManager
   - 客户端通过flink run提交作业
   - yarn-session会一直启动，不停地接收客户端提交的作业
   - 这种方式创建的Flink集群会独占资源。
   - 如果有大量的小作业/任务比较小，或者工作时间短，适合使用这种方式，减少资源创建的时间.
   (2).直接在YARN上提交运行Flink作业(Run a Flink job on YARN)
   bin/flink run -m yarn-cluster -yn 2 -yjm 1024 -ytm 1024
   /export/servers/flink/examples/batch/WordCount.jar
   # -m jobmanager的地址
   # -yn 表示TaskManager的个数
   ./flink run -m yarn-cluster
   停止yarn-cluster：
   yarn application -kill application_1527077715040_0003
   rm -rf /tmp/.yarn-properties-root
   