 1.作业提交
 
   Flink的jar文件并不是Flink集群的可执行文件，需要经过转换之后提交给集群
   转换过程：
   1、 在Flink Client中，通过反射启动jar中的main函数，生成Flink StreamGraph和JobGraph。将
JobGraph提交给Flink集群。
   2、 Flink集群收到JobGraph后，将JobGraph翻译成ExecutionGraph，然后开始调度执行，启动成功之
后开始消费数据
   总结：
   Flink的核心执行流程就是，把用户的一系列API调用，转化为StreamGraph -- JobGraph --
ExecutionGraph -- 物理执行拓扑(Task DAG)
   PipelineExecutor：流水线执行器：
   是Flink Client生成JobGraph之后，将作业提交给集群运行的重要环节
   
   Session模式：AbstractSessionClusterExecutor
   Per-Job模式：AbstractJobClusterExecutor
   IDE调试：LocalExecutor
   
   Session模式：
   作业提交通过: yarn-session.sh脚本
   在启动脚本的时候检查是否已经存在已经启动好的Flink-Session模式的集群，
   然后在PipelineExecutor中，通过Dispatcher提供的Rest接口提交Flink JobGraph
   Dispatcher为每一个作业提供一个JobMaser，进入到作业执行阶段
   
   Per-Job模式：一个作业一个集群，作业之间相互隔离。
   在PipelineExecutor执行作业提交的时候，可以创建集群并将JobGraph以及所有需要的文件一起提交给
Yarn集群，在Yarn集群的容器中启动Flink Master（JobManager进程），进行初始化后，从文件系统
中获取JobGraph，交给Dispatcher，之后和Session流程相同。
   流图：